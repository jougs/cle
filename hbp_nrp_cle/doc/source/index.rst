Closed Loop Engine (CLE)
==============================================

Welcome to Closed Loop Engine's documentation!

Contents:

.. toctree::
   :maxdepth: 2

   architecture/index
   tutorials/index
   manual/index
   python_api
   glossary

