#!/bin/bash
set -e
set -o
set -x

# Debug printing
whoami
env | sort

if [ -z ${WORKSPACE} ]; then
    echo "USAGE: The WORKSPACE variable not specified"
    exit 1
fi

# Force the build to happen in the build workspace, not in the container
# This should be done at the beginning, but there are some circular dependencies
# between Gazebo build artifacts (existing in the container) and nrp aliases/variables
# TODO: the sourcing should be made twice

export HBP=$WORKSPACE
cd $WORKSPACE
export PYTHONPATH=
source ${USER_SCRIPTS_DIR}/nrp_variables
source ${USER_SCRIPTS_DIR}/nrp_aliases

cd ${WORKSPACE}/${GIT_CHECKOUT_DIR}

# Checkout config.ini.sample from user-scripts

cp ${WORKSPACE}/${USER_SCRIPTS_DIR}/config_files/CLE/config.ini.sample hbp_nrp_cle/hbp_nrp_cle/config.ini

export PYTHONPATH=hbp_nrp_cle:$PYTHONPATH

# Concatenate all build requirements, ensure newline in between
(echo; cat ${WORKSPACE}/${EXP_CONTROL_DIR}/hbp_nrp_excontrol/requirements.txt) >> hbp_nrp_cle/requirements.txt

# Configure build

export NRP_INSTALL_MODE=dev
export IGNORE_LINT='platform_venv|hbp_nrp_cle/hbp_nrp_cle/bibi_config/generated|hbp_nrp_cle/.eggs|nest'
export MPLBACKEND=agg

# python-tk is needed by matplotlib, which is needed by csa, which is required by spynnaker8
# MPLBACKEND tells matplotlib not to use python-tk, so we can install spynnaker later without having python-tk


# devinstall is run in order to fail build in case installation/requirements fails
# then continue make to the end ignoring errors (-i)
virtualenv build_env \
    && . build_env/bin/activate \
    && pip install mock testfixtures matplotlib \
    && make devinstall \
    && make verify_base -i
