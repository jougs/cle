***********
User Manual
***********

.. todo:: Add author/responsible

.. toctree::
   :maxdepth: 2

   installing
   running
   transfer_functions
   device_configurations
   hard_reset
   access_neuron_parameters